var votersQty = $('#votersQty');
var hourlyTime = $('#hourlyTime');
var closedTime = $('#closedTime');
var poolsNum = $('#poolsNum');
var unAuth = $('#unAuth');
var violDesc = $('#violDesc');
var candidates = $('#candidates');
var type = $('#type');

$(document).ready(function () {

    if (type.val() !== "") {
        showElement(type.val())
    }

    type.change(function () {
        var value = $(this).val();
        showElement(value);
    });

    function showElement(value) {
        votersQty.hide();
        hourlyTime.hide();
        closedTime.hide();
        unAuth.hide();
        violDesc.hide();
        poolsNum.hide();
        candidates.hide();

        if (value == 'hourlyQuantity') {
            votersQty.show();
            hourlyTime.show();
            poolsNum.show();
        }

        if (value == 'finalResult') {
            votersQty.show();
            closedTime.show();
            poolsNum.show();
        }

        if (value == 'nonVoted') {
            votersQty.show();
            closedTime.show();
            poolsNum.show();
        }

        if (value == 'homeVoted') {
            votersQty.show();
            poolsNum.show();
        }

        if (value == 'startCalculation') {
            closedTime.show();
            poolsNum.show();
        }

        if (value == 'violations') {
            unAuth.show();
            violDesc.show();
            poolsNum.show();
        }

        if (value == 'finalVotes') {
            candidates.show();
            poolsNum.show();
        }
    }
});