CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profilesId` int(10) unsigned NOT NULL,
  `resource` varchar(16) NOT NULL,
  `action` varchar(16) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `profilesId` (`profilesId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=89 ;


INSERT INTO `permissions` (`id`, `profilesId`, `resource`, `action`) VALUES

('', 2, 'volunteer', 'index'),
('', 2, 'volunteer', 'message'),
('', 2, 'volunteer', 'addMessage'),

('', 3, 'users', 'index'),
('', 3, 'users', 'search'),
('', 3, 'users', 'edit'),
('', 3, 'users', 'create'),
('', 3, 'users', 'delete'),
('', 3, 'users', 'changePassword'),
('', 3, 'profiles', 'index'),
('', 3, 'profiles', 'search'),
('', 3, 'profiles', 'edit'),
('', 3, 'profiles', 'create'),
('', 3, 'profiles', 'delete'),
('', 3, 'materials', 'index'),
('', 3, 'materials', 'list'),
('', 3, 'materials', 'publish'),
('', 3, 'volunteer', 'index'),
('', 3, 'volunteer', 'message'),
('', 3, 'volunteer', 'showMessage'),
('', 3, 'volunteer', 'addMessage')
