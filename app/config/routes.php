<?php
/*
 * Define custom routes. File gets included in the router service definition.
 */
$router = new Phalcon\Mvc\Router();

$router->add('/confirm/{code}/{email}', array(
    'controller' => 'user_control',
    'action' => 'confirmEmail'
));

$router->add('/reset-password/{code}/{email}', array(
    'controller' => 'user_control',
    'action' => 'resetPassword'
));

$router->addPost('/api/sms', array(
    'controller' => 'api',
    'action' => 'sms'
));
$router->addGet('/api/phones', array(
    'controller' => 'api',
    'action' => 'phones'
));

return $router;
