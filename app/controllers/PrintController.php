<?php
namespace Elections\Controllers;

use Elections\Models\Users;

/**
 * Display the default index page.
 */
class PrintController extends ControllerBase
{

    /**
     * Default action. Set the public layout (layouts/public.volt)
     */
    public function initialize()
    {
        $this->view->setTemplateBefore('print');
    }


    public function volunteerAction()
    {
        $users = Users::find(array(
            'condition' => "active = 'Y' AND profilesId = 2"
        ));

        $count = Users::count(array(
            'condition' => "active = 'Y' AND profilesId = 2"
        ));


        $this->view->users = $users;
        $this->view->count = $count;
    }


    public function statementAction()
    {
        $identity = $this->auth->getIdentity();

        $this->view->user = Users::findFirst($identity['id']);
    }
}
