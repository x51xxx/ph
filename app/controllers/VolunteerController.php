<?php

namespace Elections\Controllers;

use Elections\Forms\MessageFilterForm;
use Elections\Forms\MessageForm;
use Elections\Forms\PhotoForm;
use Elections\Models\ElectionsInfo;
use Elections\Models\Polls;
use Elections\Models\Sms;
use Elections\Models\Users;
use Elections\Models\Voters;
use Elections\Models\Votes;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Paginator\Adapter\NativeArray as PaginatorArray;
use Phalcon\Tag;
use Phalcon\Validation;
use PHPExcel_Style_Border;
use Zend\Http\Response;

class VolunteerController extends ControllerBase
{

    public function initialize()
    {
        $this->view->setTemplateBefore('mobile');
    }

    public function indexAction()
    {
        $form = new PhotoForm(null);

        if ($this->request->isPost()) {
            $identity = $this->auth->getIdentity();
            $user = Users::findFirst($identity['id']);

            if ($form->isValid(array('csrf' => $this->security->getSessionToken()))
                && $this->request->hasFiles()) {
                foreach ($this->request->getUploadedFiles() as $file) {

                    $fileInfo = new \SplFileInfo($file->getName());

                    $fileName = $identity['id'] . '_' . time() . '.' . $fileInfo->getExtension();
                    $uploadFile = __DIR__ . '/../../public/files/' . $fileName;
                    $type = $file->getRealType();

                    if ($file->moveTo($uploadFile)) {
                        $fileItem = new \Elections\Models\Files();

                        $fileItem->assign(array(
                            'description' => $this->request->getPost('description', 'striptags'),
                            'important' => $this->request->getPost('important', 'string'),
                            'eventTypeId' => $this->request->getPost('eventTypeId'),
                            'pollsId' => $this->request->getPost('pollsId', 'int', $user->pollsId),
                            'type' => $type,
                            'file' => $fileName,
                            'userId' => $user->id,
                            'createdAt' => time()
                        ));

                        if (!$fileItem->save()) {
                            foreach ($fileItem->getMessages() as $message) {
                                $this->flash->error($message);
                            }
                        } else {
                            Tag::resetInput();
                            $this->flash->success("Матеріал збережено");
                        }
                    }
                }
            } else {
                foreach ($form->getMessages() as $message) {
                    $this->flash->error($message);
                }
            }
        }

        $this->view->form = $form;
    }

    public function addMessageAction()
    {
        $form = new MessageForm();

        if ($this->request->isPost()) {
            $validation = new Validation();
            $formElements = array();

            $type = $this->request->getPost('type', 'string');

            if ($type == 'hourlyQuantity') {
                $formElements = array('quantity', 'timeAt', 'pollsId', 'csrf');
            } elseif ($type == 'finalResult') {
                $formElements = array('quantity', 'hours', 'minutes', 'pollsId', 'csrf');
            } elseif ($type == 'nonVoted') {
                $formElements = array('quantity', 'hours', 'minutes', 'pollsId', 'csrf');
            } elseif ($type == 'homeVoted') {
                $formElements = array('quantity', 'pollsId', 'csrf');
            } elseif ($type == 'startCalculation') {
                $formElements = array('hours', 'minutes', 'pollsId', 'csrf');
            } elseif ($type == 'violations') {
                if ($this->request->getPost('unauthorizedPersons', 'string') == 'N') {
                    $formElements = array('pollsId', 'description', 'csrf');
                } else {
                    $formElements = array('pollsId', 'csrf');
                }
            } elseif ($type == 'finalVotes') {
                $formElements = array('pollsId', 'votesBOV', 'votesGAS', 'votesDMM', 'votesLOV', 'votesPPO', 'votesTUV', 'votesTSL');
            } else {
                $formElements = array('type', 'csrf');
            }

            foreach ($formElements as $element) {
                foreach ($form->get($element)->getValidators() as $validator) {
                    $validation->add($element, $validator);
                }
            }

            $csrf = array('csrf' => $this->security->getSessionToken());
            $data = array_merge($_POST, $csrf);

            if (count($validation->validate($data)) === 0) {
                $identity = $this->auth->getIdentity();

                if ($type != 'startCalculation' && $type != 'violations' && $type != 'finalVotes') {
                    $voters = new Voters();

                    $voters->quantity = $this->request->getPost('quantity', 'int');

                    $time = new \DateTime();
                    if ($this->request->getPost('timeAt', 'int') !== "") {
                        $hour = $this->request->getPost('timeAt', 'int');
                        $time->setTime($hour, 00);
                        $voters->timeAt = $time->getTimestamp();
                        $testTime = $time->format('Y-m-d H:i');
                    } else {
                        $hour = $this->request->getPost('hours', 'int');
                        $minute = $this->request->getPost('minutes', 'int');
                        $time->setTime($hour, $minute);
                        $voters->timeAt = $time->getTimestamp();
                        $testTime = $time->format('Y-m-d H:i');
                    }

                    $timeAdded = new \DateTime('now', new \DateTimeZone('Europe/Kiev'));
                    $voters->timeAdded = $timeAdded->getTimestamp();
                    $voters->hour = $hour;
                    $voters->pollsId = $this->request->getPost('pollsId', 'int');
                    $voters->usersId = $identity['id'];

                    if ($type == 'finalResult') {
                        $voters->isFinalResult = 'Y';
                    }

                    if ($type == 'nonVoted') {
                        $voters->isNoneVoted = 'Y';
                    }

                    if ($type == 'homeVoted') {
                        $voters->isHomeVoted = 'Y';
                    }

                    if ($voters->save()) {
                        Tag::resetInput();
                        $this->flash->success('Повідомлення збережено успішно');
                    } else {
                        $this->flash->error('Не вдалося зберегти повідомлення');
                    }
                } elseif ($type != 'finalVotes') {
                    $electionsInfo = new ElectionsInfo();

                    $time = new \DateTime();
                    $hour = $this->request->getPost('hours', 'int', $time->format('H'));
                    $minute = $this->request->getPost('minutes', 'int', $time->format('i'));
                    $time->setTime($hour, $minute);
                    $electionsInfo->time = $time->getTimestamp();

                    if ($type == 'startCalculation') {
                        $electionsInfo->isCalculation = 'Y';
                        $electionsInfo->isViolation = 'N';
                    }

                    $electionsInfo->isUnauthorizedPersons = $this->request->getPost('unauthorizedPersons', 'string', 'N');
                    $electionsInfo->violationDescription = $this->request->getPost('description', 'string');
                    $electionsInfo->usersId = $identity['id'];
                    $electionsInfo->pollsId = $this->request->getPost('pollsId', 'int');

                    if ($electionsInfo->save()) {
                        Tag::resetInput();
                        $this->flash->success('Повідомлення збережено успішно');
                    } else {
                        $this->flash->error('Не вдалося зберегти повідомлення');
                    }
                } else {
                    $notSaved = false;
                    $this->db->begin();

                    $votesQty = array();
                    $votesQty[] = $this->request->getPost('votesBOV', 'int');
                    $votesQty[] = $this->request->getPost('votesGAS', 'int');
                    $votesQty[] = $this->request->getPost('votesDMM', 'int');
                    $votesQty[] = $this->request->getPost('votesLOV', 'int');
                    $votesQty[] = $this->request->getPost('votesPPO', 'int');
                    $votesQty[] = $this->request->getPost('votesTUV', 'int');
                    $votesQty[] = $this->request->getPost('votesTSL', 'int');

                    for ($i = 0; $i < count($votesQty); $i++) {
                        $votes = 'votes' . $i;
                        $$votes = new Votes();
                        $$votes->pollsId = $this->request->getPost('pollsId', 'int');
                        $$votes->usersId = $identity['id'];
                        $$votes->votesQty = $votesQty[$i];
                        $$votes->candidateId = $i + 1;

                        if (!$$votes->save()) {
                            $this->db->rollback();
                            $notSaved = true;
                            break;
                        }
                    }

                    $this->db->commit();

                    if ($notSaved) {
                        $this->flash->error('Виникла помилка під час збереження форми спробути ще раз');
                    } else {
                        Tag::resetInput();
                        $this->flash->error('Дані збережено успішно');
                    }
                }
            } else {
                foreach ($validation->getMessages() as $message) {
                    $this->flash->error($message);
                }
            }
        }

        $this->view->form = $form;
    }

    public function showMessageAction()
    {
        $this->view->setTemplateBefore('private');
        $type = $this->dispatcher->getParam(0);

        $form = new MessageFilterForm();

        if ($type == 'hourlyQuantity') {
            $where = array(
                'isFinalResult = "N" AND isNoneVoted = "N" AND isHomeVoted = "N"',
                'order' => array('pollsId', 'timeAt'),
            );
            $title = 'Дані по дільницям про щогодинну явку';
        } elseif ($type == 'finalResult') {
            $where = array(
                'isFinalResult = "Y"',
                'order' => array('pollsId', 'timeAt'),
            );
            $title = 'Дані по дільницям про кількість зареєстрованих виборців';
        } elseif ($type == 'nonVoted') {
            $where = array(
                'isNoneVoted = "Y"',
                'order' => array('pollsId', 'timeAt'),
            );
            $title = 'Дані по дільницям про кількість виборців які не змогли проголосувати';
        } elseif ($type == 'homeVoted') {
            $where = array(
                'isHomeVoted = "Y"',
                'order' => array('pollsId', 'timeAt'),
            );
            $title = 'Дані по дільницям про кількість виборців які проголосували вдома';
        } elseif ($type == 'startCalculation') {
            $where = array(
                'isCalculation = "Y"',
                'order' => array('pollsId', 'time'),
            );
            $title = 'Дані по дільницям про початок підрахунку голосів';
        } elseif ($type == 'violations') {
            $where = array(
                'isViolation = "Y"',
                'order' => array('pollsId', 'time'),
            );
            $title = 'Дані по дільницям про порушення';
        } else {
            $where = null;
            $title = 'Дані по дільницям про кількість голосів по кандидатам';
        }

        $numberPage = 1;

        if ($this->request->isPost()) {
            $csrf = array('csrf' => $this->security->getSessionToken());
            $data = array_merge($_POST, $csrf);

            if ($form->isValid($data)) {
                $pollNumber = $this->request->getPost('pollNumber', 'string');
                $polls = Polls::findFirstByName($pollNumber);
                if ($type != 'finalVotes') {
                    $where[0] = $where[0] . ' AND pollsId = "' . $polls->id . '"';
                } else {
                    $where = 'pollsId = "' . $polls->id . '"';
                }
            } else {
                foreach ($form->getMessages() as $errorMessage) {
                    $this->flash->error($errorMessage);
                }
            }
        } else {
            $numberPage = $this->dispatcher->getParam(1);
        }

        if ($type != 'violations' && $type != 'startCalculation' && $type != 'finalVotes') {
            $messages = Voters::find($where);
        } elseif ($type != 'finalVotes') {
            $messages = ElectionsInfo::find($where);
        } else {
            $votesRes = Votes::find($where);
            $messages = array();
            foreach ($votesRes as $votes) {
                if (!in_array($messages[$votes->pollsId], $messages)
                    && !in_array($votes->polls->name, $messages[$votes->pollsId])
                    && !in_array($votes->polls->address, $messages[$votes->pollsId])
                ) {
                    $messages[$votes->pollsId] = array();
                    $messages[$votes->pollsId][] = $votes->polls->name;
                    $messages[$votes->pollsId][] = $votes->polls->address;
                }
                $messages[$votes->pollsId][] = $votes->votesQty;
            }
        }

        if ($type != 'finalVotes') {
            $paginator = new Paginator(array(
                'data' => $messages,
                'limit' => 20,
                'page' => $numberPage
            ));
        } else {
            $paginator = new PaginatorArray(array(
                'data' => $messages,
                'limit' => 20,
                'page' => $numberPage
            ));
        }

        $this->view->page = $paginator->getPaginate();
        $this->view->type = $type;
        $this->view->title = $title;
        $this->view->form = $form;
    }

    public function generateExcelAction()
    {
        $type = $this->dispatcher->getParam(0);

        $phpExcel = new \PHPExcel();
        $phpExcel->setActiveSheetIndex(0);
        $phpExcel->getDefaultStyle()->getFont()->setName('Arial');

        if ($type == 'hourlyQuantity') {
            $where = array(
                'isFinalResult = "N" AND isNoneVoted = "N" AND isHomeVoted = "N"',
                'order' => array('pollsId', 'timeAt'),
            );
            $title = 'Дані по дільницям про щогодинну явку';
        } elseif ($type == 'finalResult') {
            $where = array(
                'isFinalResult = "Y"',
                'order' => array('pollsId', 'timeAt'),
            );
            $title = 'Дані по дільницям про кількість зареєстрованих виборців';
        } elseif ($type == 'nonVoted') {
            $where = array(
                'isNoneVoted = "Y"',
                'order' => array('pollsId', 'timeAt'),
            );
            $title = 'Дані по дільницям про кількість виборців які не змогли проголосувати';
        } elseif ($type == 'homeVoted') {
            $where = array(
                'isHomeVoted = "Y"',
                'order' => array('pollsId', 'timeAt'),
            );
            $title = 'Дані по дільницям про кількість виборців які проголосували вдома';
        } elseif ($type == 'startCalculation') {
            $where = array(
                'isCalculation = "Y"',
                'order' => array('pollsId', 'time'),
            );
            $title = 'Дані по дільницям про початок підрахунку голосів';
        } elseif ($type == 'violations') {
            $where = array(
                'isViolation = "Y"',
                'order' => array('pollsId', 'time'),
            );
            $title = 'Дані по дільницям про порушення';
        } else {
            $where = null;
            $title = 'Дані по дільницям про кількість голосів по кандидатам';
        }

        if ($type != 'violations' && $type != 'startCalculation' && $type != 'finalVotes') {
            $messages = Voters::find($where);

            $phpExcel->getActiveSheet()->mergeCells('A1:D2');
            $phpExcel->getActiveSheet()->setCellValue('A1', $title);
            $phpExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true);
            $phpExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $phpExcel->getActiveSheet()->setCellValue('A3', 'Дільниця');
            $phpExcel->getActiveSheet()->setCellValue('B3', 'Явка');
            $phpExcel->getActiveSheet()->setCellValue('C3', 'Час');
            $phpExcel->getActiveSheet()->setCellValue('D3', 'Спостерігач');
            $phpExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $phpExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $phpExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
            $phpExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

            $row = 4;
            foreach ($messages as $value) {
                $phpExcel->getActiveSheet()->setCellValue('A'.$row, $value->polls->name);
                $phpExcel->getActiveSheet()->setCellValue('B'.$row, $value->quantity);
                $phpExcel->getActiveSheet()->setCellValue('C'.$row, $value->hour . ':00');
                $phpExcel->getActiveSheet()->setCellValue('D'.$row, $value->users->name);
                $row++;
            }
            $phpExcel->getActiveSheet()->getStyle('A1:D2')->getFont()->setBold(true);
            $phpExcel->getActiveSheet()->getStyle('A3:D3')->getFont()->setBold(true);
            $lastRow = $row - 1;
            $phpExcel->getActiveSheet()->getStyle('A3:D' . $lastRow)->getBorders()->getAllBorders()->setBorderStyle(true, \PHPExcel_Style_Border::BORDER_THIN);
        } elseif ($type != 'finalVotes') {
            $messages = ElectionsInfo::find($where);

            if ($type == 'startCalculation') {
                $phpExcel->getActiveSheet()->mergeCells('A1:C2');
                $phpExcel->getActiveSheet()->setCellValue('A1', $title);
                $phpExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true);
                $phpExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $phpExcel->getActiveSheet()->setCellValue('A3', 'Дільниця');
                $phpExcel->getActiveSheet()->setCellValue('B3', 'Час');
                $phpExcel->getActiveSheet()->setCellValue('C3', 'Спостерігач');
                $phpExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                $phpExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                $phpExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

                $row = 4;
                foreach ($messages as $value) {
                    $phpExcel->getActiveSheet()->setCellValue('A'.$row, $value->polls->name);
                    $phpExcel->getActiveSheet()->setCellValue('B'.$row, date('H:i', $value->time));
                    $phpExcel->getActiveSheet()->setCellValue('C'.$row, $value->users->name);
                    $row++;
                }
                $phpExcel->getActiveSheet()->getStyle('A1:C2')->getFont()->setBold(true);
                $phpExcel->getActiveSheet()->getStyle('A3:C3')->getFont()->setBold(true);
                $lastRow = $row - 1;
                $phpExcel->getActiveSheet()->getStyle('A3:C' . $lastRow)->getBorders()->getAllBorders()->setBorderStyle(true, \PHPExcel_Style_Border::BORDER_THIN);
            } else {
                $phpExcel->getActiveSheet()->mergeCells('A1:E2');
                $phpExcel->getActiveSheet()->setCellValue('A1', $title);
                $phpExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true);
                $phpExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $phpExcel->getActiveSheet()->setCellValue('A3', 'Дільниця');
                $phpExcel->getActiveSheet()->setCellValue('B3', 'Час');
                $phpExcel->getActiveSheet()->setCellValue('C3', 'Сторонні особи');
                $phpExcel->getActiveSheet()->setCellValue('D3', 'Опис порушення');
                $phpExcel->getActiveSheet()->setCellValue('E3', 'Спостерігач');
                $phpExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                $phpExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                $phpExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                $phpExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                $phpExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

                $row = 4;
                foreach ($messages as $value) {
                    $phpExcel->getActiveSheet()->setCellValue('A'.$row, $value->polls->name);
                    $phpExcel->getActiveSheet()->setCellValue('B'.$row, date('H:i', $value->time));
                    if ($value->isUnauthorizedPersons == 'Y') {
                        $phpExcel->getActiveSheet()->setCellValue('C' . $row, 'Так');
                    } else {
                        $phpExcel->getActiveSheet()->setCellValue('C' . $row, "");
                    }
                    $phpExcel->getActiveSheet()->setCellValue('D'.$row, $value->violationDescription);
                    $phpExcel->getActiveSheet()->setCellValue('E'.$row, $value->users->name);
                    $row++;
                }
                $phpExcel->getActiveSheet()->getStyle('A1:E2')->getFont()->setBold(true);
                $phpExcel->getActiveSheet()->getStyle('A3:E3')->getFont()->setBold(true);
                $lastRow = $row - 1;
                $phpExcel->getActiveSheet()->getStyle('A3:E' . $lastRow)->getBorders()->getAllBorders()->setBorderStyle(true, \PHPExcel_Style_Border::BORDER_THIN);
            }
        } else {
            $votesRes = Votes::find($where);
            $messages = array();
            foreach ($votesRes as $votes) {
                if (!in_array($messages[$votes->pollsId], $messages)
                    && !in_array($votes->polls->name, $messages[$votes->pollsId])
                    && !in_array($votes->polls->address, $messages[$votes->pollsId])
                ) {
                    $messages[$votes->pollsId] = array();
                    $messages[$votes->pollsId][] = $votes->polls->name;
                    $messages[$votes->pollsId][] = $votes->polls->address;
                }
                $messages[$votes->pollsId][] = $votes->votesQty;
            }

            $phpExcel->getActiveSheet()->mergeCells('A1:H2');
            $phpExcel->getActiveSheet()->setCellValue('A1', $title);
            $phpExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true);
            $phpExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $phpExcel->getActiveSheet()->setCellValue('A3', 'Дільниця');
            $phpExcel->getActiveSheet()->setCellValue('B3', 'Богомолець О.В.');
            $phpExcel->getActiveSheet()->setCellValue('C3', 'Гриценко А.С.');
            $phpExcel->getActiveSheet()->setCellValue('D3', 'Добкін М.М.');
            $phpExcel->getActiveSheet()->setCellValue('E3', 'Ляшко O.В.');
            $phpExcel->getActiveSheet()->setCellValue('F3', 'Порошенко П.О.');
            $phpExcel->getActiveSheet()->setCellValue('G3', 'Тимошенко Ю.В.');
            $phpExcel->getActiveSheet()->setCellValue('H3', 'Тігіпко С.Л.');
            $phpExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $phpExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $phpExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
            $phpExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
            $phpExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $phpExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
            $phpExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
            $phpExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

            $row = 4;
            foreach ($messages as $value) {
                $phpExcel->getActiveSheet()->setCellValue('A'.$row, $value[0]);
                $phpExcel->getActiveSheet()->setCellValue('B'.$row, $value[2]);
                $phpExcel->getActiveSheet()->setCellValue('C'.$row, $value[3]);
                $phpExcel->getActiveSheet()->setCellValue('D'.$row, $value[4]);
                $phpExcel->getActiveSheet()->setCellValue('E'.$row, $value[5]);
                $phpExcel->getActiveSheet()->setCellValue('F'.$row, $value[6]);
                $phpExcel->getActiveSheet()->setCellValue('G'.$row, $value[7]);
                $phpExcel->getActiveSheet()->setCellValue('H'.$row, $value[8]);
                $row++;
            }
            $phpExcel->getActiveSheet()->getStyle('A1:H2')->getFont()->setBold(true);
            $phpExcel->getActiveSheet()->getStyle('A3:H3')->getFont()->setBold(true);
            $lastRow = $row - 1;
            $phpExcel->getActiveSheet()->getStyle('A3:H' . $lastRow)->getBorders()->getAllBorders()->setBorderStyle(true, \PHPExcel_Style_Border::BORDER_THIN);
        }


        $phpExcel->getProperties()->setCreator('kvu.if.ua');
        $phpExcel->getProperties()->setTitle($title);
        $time = new \DateTime('now', new \DateTimeZone('Europe/Kiev'));
        $fileName = 'dataExport'. $time->format('ymdHis') .'.xlsx';

        header('Content-Type: application/vnd.openxmlformats-
officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'. $fileName .'"');
        header('Cache-Control: max-age=0');

        $excelWriter = \PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');
        $excelWriter->save('php://output');
        $phpExcel->disconnectWorksheets();
        unset($phpExcel);
    }
} 