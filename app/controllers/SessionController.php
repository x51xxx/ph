<?php
namespace Elections\Controllers;

use Elections\Forms\LoginForm;
use Elections\Forms\SignUpForm;
use Elections\Forms\ForgotPasswordForm;
use Elections\Auth\Exception as AuthException;
use Elections\Models\Users;
use Elections\Models\ResetPasswords;

/**
 * Controller used handle non-authenticated session actions like login/logout, user signup, and forgotten passwords
 */
class SessionController extends ControllerBase
{

    /**
     * Default action. Set the public layout (layouts/public.volt)
     */
    public function initialize()
    {
        $this->view->setTemplateBefore('public');
    }

    public function indexAction()
    {

    }

    /**
     * Allow a user to signup to the system
     */
    public function signupAction()
    {

        $form = new SignUpForm();

        if ($this->request->isPost()) {

            if ($form->isValid($this->request->getPost()) != false) {

                $user = new Users();

                $user->assign(array(
                    'name' => $this->request->getPost('name', 'striptags'),
                    'email' => $this->request->getPost('email'),
                    'phone' => $this->request->getPost('phone'),
                    'addressOfResidence' => $this->request->getPost('addressOfResidence'),
                    'placeOfWork' => $this->request->getPost('placeOfWork'),
                    'work' => $this->request->getPost('work'),
                    'birthday' => $this->request->getPost('birthday'),
                    'pollsId' => $this->request->getPost('pollsId'),
                    'password' => $this->security->hash($this->request->getPost('password')),
                    'profilesId' => 2
                ));
//
//                $files = [];
//                if ($this->request->hasFiles()) {
//                    foreach($this->request->getUploadedFiles() as $key => $file){
//                        $files[$file->getKey()] = $file;
//
//                        $fileInfo = new \SplFileInfo($file->getName());
//                        $fileName = '_' . time() . '.' . $fileInfo->getExtension();
//                        $uploadFile = __DIR__ . '/../../public/files/scans/' . $fileName;
//                        $type = $file->getRealType();
//
//                        if ($file->moveTo($uploadFile)) {
//                    }
//                }


                if ($user->save()) {
                    return $this->dispatcher->forward(array(
                        'controller' => 'volunteer',
                        'action' => 'index'
                    ));
                }

                $this->flash->error($user->getMessages());
            }
        }

        $this->view->form = $form;
    }

    /**
     * Starts a session in the admin backend
     */
    public function loginAction()
    {

        $form = new LoginForm();

        try {

            if (!$this->request->isPost()) {

                if ($this->auth->hasRememberMe()) {
                    return $this->auth->loginWithRememberMe();
                }
            } else {

                if ($form->isValid($this->request->getPost()) == false) {
                    foreach ($form->getMessages() as $message) {
                        $this->flash->error($message);
                    }
                } else {

                    $this->auth->check(array(
                        'email' => $this->request->getPost('email'),
                        'password' => $this->request->getPost('password'),
                        'remember' => $this->request->getPost('remember')
                    ));

                    $identity = $this->auth->getIdentity();
                    $user = Users::findFirst($identity['id']);
                    if( $user->profilesId == '3' ){
                        return $this->response->redirect('materials/list');
                    }else{
                        return $this->response->redirect('volunteer');
                    }
                }
            }
        } catch (AuthException $e) {
            $this->flash->error($e->getMessage());
        }

        $this->view->form = $form;
    }

    /**
     * Shows the forgot password form
     */
    public function forgotPasswordAction()
    {

        $form = new ForgotPasswordForm();

        if ($this->request->isPost()) {

            if ($form->isValid($this->request->getPost()) == false) {
                foreach ($form->getMessages() as $message) {
                    $this->flash->error($message);
                }
            } else {

                $user = Users::findFirstByEmail($this->request->getPost('email'));
                if (!$user) {
                    $this->flash->success('Немає облікового запису, пов\'язані з цією електронною поштою');
                } else {

                    $resetPassword = new ResetPasswords();
                    $resetPassword->usersId = $user->id;
                    if ($resetPassword->save()) {
                        $this->flash->success('Success! Please check your messages for an email reset password');
                    } else {
                        foreach ($resetPassword->getMessages() as $message) {
                            $this->flash->error($message);
                        }
                    }
                }
            }
        }

        $this->view->form = $form;
    }

    /**
     * Closes the session
     */
    public function logoutAction()
    {
        $this->auth->remove();

        return $this->response->redirect('session/login');
    }
}
