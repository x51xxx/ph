<?php

namespace Elections\Controllers;


use Elections\Models\Files;
use Phalcon\Tag;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class MaterialsController extends ControllerBase
{

    public function  initialize()
    {
        $this->view->setTemplateBefore('private');
    }

    public function indexAction()
    {
       return $this->response->redirect('materials/list');
    }

    public function listAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, 'Elections\Models\Files', $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = array();
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $parameters['order'] = "id DESC";

        $files = Files::find($parameters);
//        if (count($users) == 0) {
//            $this->flash->notice("The search did not find any users");
//            return $this->dispatcher->forward(array(
//                "action" => "index"
//            ));
//        }

        $paginator = new  Paginator(array(
            "data" => $files,
            "limit" => 20,
            "page" => $numberPage
        ));

        $this->view->blogId = $this->config->blogger->id;
        $this->view->page = $paginator->getPaginate();
    }


    public function publishAction($id)
    {
        $file = Files::findFirst($id);
        if (!$file) {
            $this->flash->error("File was not found");
            return $this->dispatcher->forward(array(
                'action' => 'index'
            ));
        }

        $user = $this->config->blogger->username;
        $pass = $this->config->blogger->password;
        $blogID = $this->config->blogger->id;

        $adapter = new \ZendGData\HttpClient();
        $adapter->setAdapter(new \Zend\Http\Client\Adapter\Curl());

        try {
            $client = \ZendGData\ClientLogin::getHttpClient($user, $pass, 'blogger', $adapter,
                \ZendGData\ClientLogin::DEFAULT_SOURCE, null, null,
                \ZendGData\ClientLogin::CLIENTLOGIN_URI, 'GOOGLE');
        } catch (\ZendGData\App\AuthException $ae) {
            echo 'Problem authenticating: ' . $ae->exception() . "\n";
        }

        $gdClient = new \ZendGData\GData($client);
        $gdClient->setMajorProtocolVersion(2);

        $title = 'Документ';
        $content = $file->description;
        $content .= '<br/>';

        $domain = $this->config->application->publicUrl;

        $fileInfo = new \SplFileInfo($file->file);

        if (in_array( strtolower($fileInfo->getExtension()), array("png", "jpeg", "gif", "jpg", "tiff"))) {
            $content .= '<a href="http://'.$domain.'/files/'.$file->file.'" target="_blank">
                    <img src="http://'.$domain.'/files/'.$file->file.'" alt="" style="max-width: 800px"/>
                </a>';
        } else {
            $content .= '   <a href="http://'.$domain.'/files/'.$file->file.'" target="_blank">Переглянути</a>';
        }


        $uri = 'http://www.blogger.com/feeds/' . $blogID . '/posts/default';
        $entry = $gdClient->newEntry();
        $entry->title = $gdClient->newTitle($title);
        $entry->content = $gdClient->newContent($content);
        $entry->content->setType('html');


        //Checks if insertEntry throws any exceptions
        try {
            $createdPost = $gdClient->insertEntry($entry, $uri);
        } catch (\ZendGData\App\AuthException $ae) {
            echo 'Problem authenticating: ' . $ae->exception() . "\n";
        }

        $idText = explode('-', $createdPost->id->text);
        $newPostID = $idText[2];

        $file->postId = $newPostID;
        $file->save();

        $this->view->postId = $newPostID;
        $this->view->blogId = $blogID;
        $this->view->domain = $domain;

    }
} 