<?php

namespace Elections\Controllers;

use Elections\Models\Polls;
use Elections\Models\Users;
use Elections\Models\Voters;
use Phalcon\Mvc\Controller;
use Elections\Models\Sms;
use Zend\Http\Response;

class ApiController extends Controller
{

    public function initialize()
    {
        $keys = (array)$this->config->api->keys;

        if (empty($keys) || !in_array($this->request->get('key', 'string'), $keys)) {
            die('Bad auth');
        }
    }

    /**
     * call POST /api/sms
     *
     * @return \Phalcon\Http\Response
     */
    public function smsAction()
    {
        $response = new \Phalcon\Http\Response();
        $response->setHeader('Access-Control-Allow-Origin', '*');
        $response->setHeader('Content-Type', 'application/json');

        $sms = new Sms();
        $sms->assign(array(
            'phone' => $this->request->getPost('phone', 'string'),
            'text' => $this->request->getPost('text', 'string'),
            'receivedTime' => round($this->request->getPost('time', 'string', time() * 1000)) / 1000,
            'createdAt' => time()
        ));

        if (!$sms->save()) {
            $response->setStatusCode('400', "Bad Request");

            $errors = array();
            foreach ($sms->getMessages() as $message) {
                $errors[] = $message;
            }

            $body = array(
                "status" => "BAD_REQUEST",
                "error_messages" => $errors,
                "results" => array()
            );
        } else {
            $response->setStatusCode('200', "OK");

            $body = array(
                "status" => "OK",
                "results" => array('id' => $sms->id)
            );

            $user = Users::findFirstByPhone($sms->phone);
            if ($user) {
                $text = $sms->text;
                if (preg_match('/[KkКк\?](\d{1,2}) (\d+)/i', $text, $match)) {
                    $this->saveVoters($user, $match[1], $match[2]);
                }elseif (preg_match('/[KkКк\?]\s+(\d{1,2})[\.\-\,\:]00\s+(\d+)/i', $text, $match)) {
                    $this->saveVoters($user, $match[1], $match[2]);
                }elseif (preg_match('/[KkКк\?]\s(\d{1,2})\s+(\d+)/i', $text, $match)) {
                    $this->saveVoters($user, $match[1], $match[2]);
                }elseif (preg_match('/[KkКк\?](\d{1,2})\s+(\d+)/i', $text, $match)) {
                    $this->saveVoters($user, $match[1], $match[2]);
                } elseif (preg_match('/[ZzЗз\?] (\d+)/i', $text, $match)) {
                    $voters = new  Voters();
                    $voters->usersId = $user->id;
                    $voters->pollsId = $user->pollsId;
                    $voters->quantity = (int)$match[1];
                    $voters->hour = 8;

                    $time = new \DateTime();
                    $time->setTime(8, 00);
                    $voters->timeAt = $time->getTimestamp();
                    $voters->timeAdded = time();
                    $voters->isFinalResult = 'Y';

                    if ($voters->save()) {

                    }
                }
            }

        }

        $response->setJsonContent($body);
        return $response;
    }


    protected function saveVoters($user, $time, $count){

        $voters = new  Voters();
        $voters->usersId = $user->id;
        $voters->pollsId = $user->pollsId;
        $voters->quantity = $count;
        $voters->hour = $time;

        $time = new \DateTime();

        $time->setTime($time, 00);
        $voters->timeAt = $time->getTimestamp();
        $voters->timeAdded = time();
        $voters->isFinalResult = 'N';

        if ($voters->save()) {

        }
    }

    /*
     * call Get /api/phones
     *
     * @return \Phalcon\Http\Response
     */
    public function phonesAction()
    {
        $response = new \Phalcon\Http\Response();
        $response->setHeader('Access-Control-Allow-Origin', '*');
        $response->setHeader('Content-Type', 'application/json');

        $response->setStatusCode('200', "OK");

        $users = Users::find("profilesId IN(2,3)  AND active = 'Y' AND banned = 'N' ");

        $phones = array();
        if ($users)
            foreach ($users as $user)
                if (!empty($user->phone))
                    $phones[] = $user->phone;

        $body = array(
            "status" => "OK",
            "results" => array('phones' => $phones)
        );

        $response->setJsonContent($body);
        return $response;
    }

    public function exportAction()
    {
        $file = file(__DIR__ . "/../../data/users.csv");

        foreach ($file as $line) {

            $data = explode(",", $line);

            if (count($data)) {

                $data = array_filter($data, function ($e) {
                    return trim($e);
                });

                $pool = Polls::findFirstByName($data[0]);
                echo $data[2], ' ', $data[4], PHP_EOL;


                $user = new Users();

                $user->assign(array(
                    "name" => $data[1],
                    "pollsId" => $pool->id,
                    "active" => "Y",
                    "password" => $this->security->hash((string)$data[4]),
                    "email" => $data[3],
                    "phone" => $data[2],
                    "profilesId" => 2,
                    "addressOfResidence" => " ",
                    "placeOfWork" => " ",
                    "work" => " ",
                ));

                if (!$user->save()) {
                    foreach ($user->getMessages() as $message) {
                        echo $message, "\n";
                    }
                }

            }

        }
        exit;

    }
} 