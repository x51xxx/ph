<?php

namespace Elections\Models;


use Phalcon\Mvc\Model;

/**
 * Class Votes
 * @namespace Elections\Models
 */
class Votes extends Model
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $votesQty;

    /**
     * @var int
     */
    public $candidateId;

    /**
     * @var int
     */
    public $pollsId;

    /**
     * @var int
     */
    public $usersId;

    public function initialize()
    {
        $this->belongsTo('candidateId', 'Elections\Models\Candidates', 'id', array(
            'alias' => 'candidates',
            'reusable' => true
        ));

        $this->belongsTo('pollsId', 'Elections\Models\Polls', 'id', array(
            'alias' => 'polls',
            'reusable' => true
        ));

        $this->belongsTo('usersId', 'Elections\Models\Users', 'id', array(
            'alias' => 'users',
            'reusable' => true
        ));
    }
} 