<?php
namespace Elections\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Behavior\Timestampable;

/**
 * Elections\Models\Users
 * All the users registered in the application
 */
class Files extends Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $description;

    /**
     *
     * @var string
     */
    public $file;


    /**
     *
     * @var integer
     */
    public $userId;

    /**
     *
     * @var string
     */
    public $createdAt;


    /**
     *
     * @var boolean
     */
    public $important;


    /**
     *
     * @var string
     */
    public $type;


    /**
     *
     * @var int
     */
    public $eventTypeId;


    /**
     *
     * @var string
     */
    public $postId;


    public function initialize()
    {
        $this->addBehavior(new Timestampable(
            array(
                'beforeCreate' => array(
                    'field' => 'createdAt'
                )
            )
        ));

        $this->belongsTo('userId', 'Elections\Models\Users', 'id', array(
            'alias' => 'user'
        ));

        $this->belongsTo('eventTypeId', 'Elections\Models\EventType', 'id', array(
            'alias' => 'eventType'
        ));
    }
}
