<?php
namespace Elections\Models;


use Phalcon\Mvc\Model;

/**
 * Class ElectionsInfo
 * @namespace Elections\Models
 * Contains all info about violations
 */
class ElectionsInfo extends Model
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $time;

    /**
     * @var string
     */
    public $isViolation = 'Y';

    /**
     * @var string
     */
    public $violationDescription;

    /**
     * @var string
     */
    public $isCalculation = 'N';

    /**
     * @var string
     */
    public $isUnauthorizedPersons = 'N';

    /**
     * @var int
     */
    public $pollsId;

    /**
     * @var int
     */
    public $usersId;

    /**
     * Define relationships to Users and ElectionInfo
     */
    public function initialize()
    {
        $this->belongsTo('usersId', 'Elections\Models\Users', 'id', array(
            'alias' => 'users',
            'reusable' => true
        ));

        $this->belongsTo('pollsId', 'Elections\Models\Polls', 'id', array(
            'alias' => 'polls',
            'reusable' => true
        ));
    }
} 