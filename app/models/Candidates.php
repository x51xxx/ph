<?php

namespace Elections\Models;


use Phalcon\Mvc\Model;

/**
 * Class Candidates
 * @namespace Elections\Models
 */
class Candidates extends Model
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    public function initialize()
    {
        $this->hasMany('id', 'Elections\Models\Votes', 'candidateId', array(
            'alias' => 'candidates',
            'foreignKey' => array(
                'message' => 'Кандидат не може бути видалений тому що його/її дані використовуються в системі'
            )
        ));
    }
} 