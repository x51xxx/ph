<?php
namespace Elections\Models;

use Phalcon\Mvc\Model;


class EventType extends Model
{

    /**
     *
     * @var integer
     */
    public $id;


    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $active;




    public function getSource()
    {
        return 'event_type';
    }

}
