<?php
namespace Elections\Models;

use Phalcon\Mvc\Model;


class Sms extends Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $createdAt;

    /**
     *
     * @var string
     */
    public $text;

    /**
     *
     * @var string
     */
    public $phone;

}
