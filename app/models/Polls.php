<?php
namespace Elections\Models;

use Phalcon\Mvc\Model;

class Polls extends Model
{

    /**
     * ID
     * @var integer
     */
    public $id;

    /**
     * Name
     * @var string
     */
    public $name;

    /**
     * active
     * @var string
     */
    public $active;

    /**
     * Name
     * @var string
     */
    public $address;

    /**
     * Number Of Voters
     * @var integer
     */
    public $numberOfVoters;

    /**
     * Number Of Voters
     * @var integer
     */
    public $district;

    /**
     * Define relationships to Users and Permissions
     */
    public function initialize()
    {

    }

    public function __toString(){
        return $this->name;
    }
}
