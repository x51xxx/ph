<?php
namespace Elections\Models;


use Phalcon\Mvc\Model;

/**
 * Class Voters
 * @namespace Elections\Models
 * All quantities from election districts
 */
class Voters extends Model
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $quantity;

    /**
     * @var int
     */
    public $timeAt;

    /**
     * @var int
     */
    public $timeAdded;

    /**
     * @var int
     */
    public $hour;

    /**
     * @var int
     */
    public $pollsId;

    /**
     * @var int
     */
    public $usersId;

    /**
     * @var string
     */
    public $isFinalResult = 'N';

    /**
     * @var string
     */
    public $isNoneVoted = 'N';

    /**
     * @var string
     */
    public $isHomeVoted = 'N';

    /**
     * Define relationships to Users and Voters
     */
    public function initialize()
    {
        $this->belongsTo('usersId', 'Elections\Models\Users', 'id', array(
            'alias' => 'users',
            'reusable' => true
        ));

        $this->belongsTo('pollsId', 'Elections\Models\Polls', 'id', array(
            'alias' => 'polls',
            'reusable' => true
        ));
    }
} 