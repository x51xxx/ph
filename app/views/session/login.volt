{{ content() }}

<div align="center" class="well">

    {{ form('class': 'form-search') }}

    <div align="left">
        <h2>Вхід</h2>
    </div>

    <div class="clearfix">
        {{ form.render('email') }}
    </div>
    <div class="clearfix">
        {{ form.render('password') }}
    </div>
    <div class="clearfix">
        {{ form.render('go') }}
    </div>
    <div align="center" class="remember">
        {{ form.render('remember') }}
        {{ form.label('remember') }}
    </div>

    {{ form.render('csrf', ['value': security.getToken()]) }}

    <hr>

    <div class="forgot">
        {{ link_to("session/forgotPassword", "Відновити пароль") }}
    </div>

    </form>

</div>