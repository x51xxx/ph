{{ content() }}

<div align="center">

	{{ form('class': 'form-search',  'enctype':"multipart/form-data") }}

		<div align="left">
			<h2>Реєстрація</h2>
		</div>

		<table class="signup">
			<tr>
				<td align="right">{{ form.label('name') }}</td>
				<td>
					{{ form.render('name') }}
					{{ form.messages('name') }}
				</td>
			</tr>
			<tr>
				<td align="right">{{ form.label('email') }}</td>
				<td>
					{{ form.render('email') }}
					{{ form.messages('email') }}
				</td>
			</tr>
            <tr>
				<td align="right">{{ form.label('phone') }}</td>
				<td>
					{{ form.render('phone') }}
					{{ form.messages('phone') }}
				</td>
			</tr>

            <tr>
                <td align="right">{{ form.label('birthday') }}</td>
                <td>
                    {{ form.render('birthday') }}
                    {{ form.messages('birthday') }}
                </td>
            </tr>

            <tr>
				<td align="right">{{ form.label('addressOfResidence') }}</td>
				<td>
					{{ form.render('addressOfResidence') }}
					{{ form.messages('addressOfResidence') }}
				</td>
			</tr>
            <tr>
				<td align="right">{{ form.label('placeOfWork') }}</td>
				<td>
					{{ form.render('placeOfWork') }}
					{{ form.messages('placeOfWork') }}
				</td>
			</tr>
            <tr>
				<td align="right">{{ form.label('work') }}</td>
				<td>
					{{ form.render('work') }}
					{{ form.messages('work') }}
				</td>
			</tr>
            <tr>
				<td align="right">{{ form.label('pollsId') }}</td>
				<td>
					{{ form.render('pollsId') }}
					{{ form.messages('pollsId') }}
				</td>
			</tr>

               {#<tr>#}
				{#<td align="right">{{ form.label('passport_scan1') }}</td>#}
				{#<td>#}
					{#{{ form.render('passport_scan1') }}#}
					{#{{ form.messages('passport_scan1') }}#}
				{#</td>#}
			{#</tr>#}
            {#<tr>#}
				{#<td align="right">{{ form.label('passport_scan2') }}</td>#}
				{#<td>#}
					{#{{ form.render('passport_scan2') }}#}
					{#{{ form.messages('passport_scan2') }}#}
				{#</td>#}
			{#</tr>#}
            {##}
            <tr>
              <td colspan="2">
                  <hr/>
              </td>
            </tr>
			<tr>
				<td align="right">{{ form.label('password') }}</td>
				<td>
					{{ form.render('password') }}
					{{ form.messages('password') }}
				</td>
			</tr>
			<tr>
				<td align="right">{{ form.label('confirmPassword') }}</td>
				<td>
					{{ form.render('confirmPassword') }}
					{{ form.messages('confirmPassword') }}
				</td>
			</tr>
			<tr>
				<td align="right"></td>
				<td>
					{{ form.render('terms') }} {{ form.label('terms') }}
					{{ form.messages('terms') }}
				</td>
			</tr>
			<tr>
				<td align="right"></td>
				<td>{{ form.render('Зареєструватись') }}</td>
			</tr>
		</table>

		{{ form.render('csrf', ['value': security.getToken()]) }}
		{{ form.messages('csrf') }}

		<hr>

	</form>

</div>