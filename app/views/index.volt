<!DOCTYPE html>
<html>
	<head>
		<title>Вибори 2014</title>
		<link href="//netdna.bootstrapcdn.com/bootswatch/2.3.1/united/bootstrap.min.css" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		{{ stylesheet_link('css/style.css') }}
	</head>
	<body>

		{{ content() }}

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>
        {% if dispatcher.getControllerName() == 'volunteer'
            and dispatcher.getActionName() == 'addMessage'
        %}
            {{ javascript_include('js/messages.js') }}
        {% endif %}
    </body>
</html>