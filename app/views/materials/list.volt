{{ content() }}

<ul class="pager">
    <li class="previous pull-left">
        {{ link_to("docs/index", "&larr; Повернутись") }}
    </li>
    <li class="pull-right">
        {{ link_to("docs/create", "Додати документ", "class": "btn btn-primary") }}
    </li>
</ul>

{% for doc in page.items %}
    {% if loop.first %}
        <table class="table table-bordered table-striped" align="center">
        <thead>
        <tr>
            <th></th>
            <th>Документ</th>
            <th>Спостерігач</th>
            <th>Дільниця</th>
            <th>Дії</th>
        </tr>
        </thead>
    {% endif %}
    <tbody>
    <tr {% if doc.important == 'y' %}class="warning"{% endif %}>

        <td>
            <strong>{{ date('H:i:s d-m-Y', doc.createdAt) }}</strong>
            <br/>

            <br/>
            <strong>ID</strong>: {{ doc.id }}
        </td>
        <td>
            <?php

                 $fileInfo = new \SplFileInfo($doc->file);
                 $image = in_array( strtolower($fileInfo->getExtension()), array("png", "jpeg", "gif", "jpg", "tiff"));
            //     { if doc.type == "image/jpeg" or doc.type == "image/png"  or doc.type == "image/gif" }

            ?>
            {% if image %}
                <a href="/files/{{ doc.file }}" target="_blank">
                    <img src="/files/{{ doc.file }}" alt="" style="max-width: 280px"/>
                </a>
            {% else %}
                <a href="/files/{{ doc.file }}" target="_blank">Переглянути</a>
            {% endif %}

            <br/>

            {{ doc.description }}
        </td>
        <td>{{ doc.user.name }}</td>
        <td title="{{ doc.user.poll.address  }}">{{ doc.user.poll }}</td>
        <td width="16%">
            {% if doc.postId %}
                <a href="https://www.blogger.com/blogger.g?blogID={{ blogId }}#editor/target=post;postID={{ doc.postId }}">Редагувати</a>
            {% else %}
                {{ link_to("materials/publish/" ~ doc.id, '<i class="icon-pencil"></i> Публікувати', "class": "btn") }}
            {% endif %}
        </td>
        {#<td width="16%">{{ link_to("materials/delete/" ~ doc.id, '<i class="icon-remove"></i> Видалити', "class": "btn") }}</td>#}
    </tr>
    </tbody>
    {% if loop.last %}
        <tbody>
        <tr>
            <td colspan="10" align="right">
                <div class="btn-group">
                    {{ link_to("materials/list", '<i class="icon-fast-backward"></i> Перша', "class": "btn") }}
                    {{ link_to("materials/list?page=" ~ page.before, '<i class="icon-step-backward"></i> Попередня', "class": "btn ") }}
                    {{ link_to("materials/list?page=" ~ page.next, '<i class="icon-step-forward"></i> Наступна', "class": "btn") }}
                    {{ link_to("materials/list?page=" ~ page.last, '<i class="icon-fast-forward"></i> Остання', "class": "btn") }}
                    <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span>
                </div>
            </td>
        </tr>
        <tbody>
        </table>
    {% endif %}
{% else %}
    No docs are recorded
{% endfor %}
