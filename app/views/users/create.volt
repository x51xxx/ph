
<form method="post" autocomplete="off">

<ul class="pager">
    <li class="previous pull-left">
        {{ link_to("users", "&larr; Повернутись") }}
    </li>
    <li class="pull-right">
        {{ submit_button("Зберегти", "class": "btn btn-success") }}
    </li>
</ul>

{{ content() }}

<div class="center scaffold">
    <h2>Створення користувача</h2>

    <div class="clearfix">
        <label for="name">ПІБ</label>
        {{ form.render("name") }}
    </div>


    <div class="clearfix">
        <label for="name">Дільниця</label>
        {{ form.render("pollsId") }}
    </div>

    <div class="clearfix">
        <label for="phone">Телефон</label>
        {{ form.render("phone") }}
    </div>

    <div class="clearfix">
        <label for="email">E-Mail</label>
        {{ form.render("email") }}
    </div>

    <div class="clearfix">
        <label for="profilesId">Тип</label>
        {{ form.render("profilesId") }}
    </div>

    <div class="clearfix">
        <label for="birthday">Дата народження</label>
        {{ form.render("birthday") }}
    </div>

    <div class="clearfix">
        <label for="addressOfResidence">addressOfResidence</label>
        {{ form.render("addressOfResidence") }}
    </div>

</div>

</form>