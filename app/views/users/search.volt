{{ content() }}

<ul class="pager">
    <li class="previous pull-left">
        {{ link_to("users/index", "&larr; Повернутись") }}
    </li>
    <li class="pull-right">
        {{ link_to("users/create", "Create users", "class": "btn btn-primary") }}
    </li>
</ul>

{% for user in page.items %}
{% if loop.first %}
<table class="table table-bordered table-striped" align="center">
    <thead>
        <tr>
            <th>Id</th>
            <th>ПІБ</th>
            <th>Email</th>
            <th>Тип</th>
            <th>Заблокований?</th>
            {#<th>Suspended?</th>#}
            <th>Підтверджиний?</th>
        </tr>
    </thead>
{% endif %}
    <tbody>
        <tr>
            <td>{{ user.id }}</td>
            <td>{{ user.name }}</td>
            <td>{{ user.email }}</td>
            <td>{{ user.profile.name }}</td>
            <td>{{ user.banned == 'Y' ? 'Так' : 'Ні' }}</td>
            {#<td>{{ user.suspended == 'Y' ? 'Yes' : 'No' }}</td>#}
            <td>{{ user.active == 'Y' ? 'Так' : 'Ні' }}</td>
            <td width="16%">{{ link_to("users/edit/" ~ user.id, '<i class="icon-pencil"></i> Редагувати', "class": "btn") }}</td>
            <td width="16%">{{ link_to("users/delete/" ~ user.id, '<i class="icon-remove"></i> Видалити', "class": "btn") }}</td>
        </tr>
    </tbody>
{% if loop.last %}
    <tbody>
        <tr>
            <td colspan="10" align="right">
                <div class="btn-group">
                    {{ link_to("users/search", '<i class="icon-fast-backward"></i> Перша', "class": "btn") }}
                    {{ link_to("users/search?page=" ~ page.before, '<i class="icon-step-backward"></i> Попередня', "class": "btn ") }}
                    {{ link_to("users/search?page=" ~ page.next, '<i class="icon-step-forward"></i> Наступна', "class": "btn") }}
                    {{ link_to("users/search?page=" ~ page.last, '<i class="icon-fast-forward"></i> Остання', "class": "btn") }}
                    <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span>
                </div>
            </td>
        </tr>
    <tbody>
</table>
{% endif %}
{% else %}
    No users are recorded
{% endfor %}
