<div class="navbar">
    <div class="navbar-inner">
      <div class="container" style="width: auto;">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
        {{ link_to(null, 'class': 'brand', 'Вибори 2014')}}

          <ul class="nav pull-right">
              <li>{{ link_to('session/login', 'Вхід') }}</li>
          </ul>

        <div class="nav-collapse">
          <ul class="nav">

            {%- set menus = [
              'Головна': null,
              'Про проект': 'about'
            ] -%}

            {%- for key, value in menus %}
              {% if value == dispatcher.getControllerName() %}
              <li class="active">{{ link_to(value, key) }}</li>
              {% else %}
              <li>{{ link_to(value, key) }}</li>
              {% endif %}
            {%- endfor -%}

          </ul>


        </div><!-- /.nav-collapse -->
      </div>
    </div><!-- /navbar-inner -->
  </div>

<div class="main-container">
  {{ content() }}
</div>

{{ partial("partials/footer") }}