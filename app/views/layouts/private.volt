<div class="navbar navbar-inverse">
  <div class="navbar-inner">
    <div class="container" style="width: auto;">
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      {{ link_to(null, 'class': 'brand', 'Вибори 2014')}}
        <div class="nav-collapse">

          <ul class="nav">

            {%- set menus = [
              'Головна': null,
              'Додати документ': 'volunteer',
              'Додати повідомлення': 'volunteer/addMessage',
              'Повідомлення': '#showMessage',
              'Матеріали': 'materials/list',
              'Користувачі': 'users',
              'Профіль': 'profiles',
              'Заява': 'print/volunteer'
            ] -%}

            {% set subMenus = [
              'Щогодинна явка': 'volunteer/showMessage/hourlyQuantity',
              'Кількість зареєстрованих виборців': 'volunteer/showMessage/finalResult',
              'Кількість виборців які не змогли проголосувати': 'volunteer/showMessage/nonVoted',
              'Кількість виборців які проголосували вдома': 'volunteer/showMessage/homeVoted',
              'Початок підрахунку голосів': 'volunteer/showMessage/startCalculation',
              'Порушення': 'volunteer/showMessage/violations',
              'Результати голосування': 'volunteer/showMessage/finalVotes'
            ] %}

            {% if dispatcher.getActionName() == 'index' %}
              {% set action = null %}
            {% else %}
              {% set action = '/' ~ dispatcher.getActionName() %}
            {% endif %}
            {%- for key, value in menus %}
              {% if value == dispatcher.getControllerName() ~ action %}
                <li class="active">{{ link_to(value, key) }}</li>
              {% elseif value == '#showMessage' %}
                {% if dispatcher.getControllerName() == 'volunteer' and dispatcher.getActionName() == 'showMessage' %}
                    <li class="dropdown active">
                {% else %}
                    <li class="dropdown">
                {% endif %}
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Повідомлення <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                        {%- for key, value in subMenus %}
                            <li>{{ link_to(value, key) }}</li>
                        {%- endfor -%}
                        </ul>
                    </li>
              {% else %}
                <li>{{ link_to(value, key) }}</li>
              {% endif %}
            {%- endfor -%}

          </ul>

        <ul class="nav pull-right">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ auth.getName() }} <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li>{{ link_to('users/changePassword', 'Змінити пароль') }}</li>
            </ul>
          </li>
          <li>{{ link_to('session/logout', 'Вийти') }}</li>
        </ul>
      </div>
    </div>
  </div>
</div>

<div class="container">
  {{ content() }}
</div>