<div class="navbar">
    <div class="navbar-inner">
        <div class="container" style="width: auto;">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            {{ link_to(null, 'class': 'brand', 'Вибори 2014') }}

            <ul class="nav pull-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ auth.getName() }} <b
                                class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>{{ link_to('users/changePassword', 'Змінити пароль') }}</li>
                        {#<li>{{ link_to('print/statement', 'Заява') }}</li>#}
                    </ul>
                </li>
                <li>{{ link_to('session/logout', 'Вийти') }}</li>
            </ul>

            <div class="nav-collapse">
                <ul class="nav">

                    {%- set menus = [
                    'Додати документ': 'volunteer',
                    'Додати повідомлення': 'volunteer/addMessage'
                    ] -%}

                    {% if dispatcher.getActionName() == 'index' %}
                        {% set action = null %}
                    {% else %}
                        {% set action = '/' ~ dispatcher.getActionName() %}
                    {% endif %}
                    {%- for key, value in menus %}
                        {% if value == dispatcher.getControllerName() ~ action %}
                            <li class="active">{{ link_to(value, key) }}</li>
                        {% else %}
                            <li>{{ link_to(value, key) }}</li>
                        {% endif %}
                    {%- endfor -%}

                </ul>


            </div>



            <!-- /.nav-collapse -->
        </div>
    </div>
    <!-- /navbar-inner -->
</div>

<div class=" main-container">
    {{ content() }}
</div>


{{ partial("partials/footer") }}