{{ content() }}

<header class="jumbotron subhead" id="overview">
    <div class="hero-unit">
        <h1>Івано-Франківська обласна організація
            «Комітет виборців України»</h1>

        <p class="lead">МОБІЛІЗАЦІЯ
            чесне спостереження
            вибори 25 травня 2014 року
            в місті Івано-Франківську
        </p>

        <div align="right">
            {{ link_to('session/signup', '<i class="icon-ok icon-white"></i> Зареєструватись спостерігачем', 'class': 'btn btn-primary btn-large') }}
        </div>
    </div>
</header>

<div id="homepage" class="row">

    <div class="offset1 span5">

        <ul class="thumbnails">
            <li class="offset1 span4">
                <div class="thumbnail">
                    <a href="http://www.cvk.gov.ua/" target="_blank"><img src="/img/CVK.jpg"></a>
                </div>
            </li>
            <li class="offset1 span4">
                <div class="thumbnail">
                    <a href="http://www.cvu.org.ua/" target="_blank"><img src="/img/CVUORG.jpg"></a>
                </div>
            </li>
            {#<li class="offset1 span4">#}
                {#<div class="thumbnail">#}
                    {#<a href="http://www.vyborcom.org.ua/" target="_blank"><img src="/img/"></a>#}
                {#</div>#}
            {#</li>#}

        </ul>

    </div>


    <div class="span6">
        <h3>Поштова адреса</h3>
        <address>
            <strong>Івано - Франківська обласна організація "Комітет Виборців України"</strong><br>
            Івано-Франківськ, вул. Грушевського, 18 офіс 32<br>
            <abbr title="Телефон">Тел:</abbr> +38 (098) 707-2755
        </address>
        <address>
            <a href="https://www.facebook.com/ifoo.kvu">Facebook</a><br>
            <a href="mailto:ifoo.kvu@gmail.com">ifoo.kvu@gmail.com</a>
        </address>

    </div>

</div>
