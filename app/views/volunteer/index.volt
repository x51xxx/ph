
<form method="post" autocomplete="off" enctype="multipart/form-data">

    <ul class="pager">
        {#<li class="previous pull-left">#}
            {#{{ link_to("profiles", "&larr; Повернутись") }}#}
        {#</li>#}
        <li class="pull-right">
            {{ submit_button("Зберегти", "class": "btn-large btn-success") }}
        </li>
    </ul>

    {{ content() }}

    <div class="center scaffold" id="volunteer">

        <h2>Завантаження документу</h2>

        <ul class="nav nav-tabs">
            <li class="active"><a href="#A" data-toggle="tab">Основні</a></li>
            <li><a href="#B" data-toggle="tab">Додатково</a></li>
        </ul>

        <div class="tabbable">
            <div class="tab-content">
                <div class="tab-pane active" id="A">

                    <div class="clearfix">
                        <label for="eventTypeId">Тип повідомлення</label>
                        {{ form.render("eventTypeId") }}
                    </div>

                    <div class="clearfix well">
                        <label for="file">Файл</label>
                        {{ form.render("file") }}
                        <p class="help-block">Фото, документи або відео.</p>
                    </div>



                    <div class="clearfix">
                        <label for="description">Опис</label>
                        {{ form.render("description") }}
                    </div>


                </div>

                <div class="tab-pane" id="B">

                    <div class="clearfix">
                        <label for="important">
                            {{ form.render("important") }}
                            Важлива інформація?
                        </label>
                    </div>

                    <hr/>
                    <div class="clearfix">
                        <label for="pollsId">Дільниця</label>
                        {{ form.render("pollsId") }}
                    </div>

                </div>

            </div>
        </div>

    </div>

</form>
