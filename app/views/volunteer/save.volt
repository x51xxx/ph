
<form method="post" autocomplete="off" enctype="multipart/form-data">

    <ul class="pager">
        <li class="previous pull-left">
            {{ link_to("profiles", "&larr; Повернутись") }}
        </li>
        <li class="pull-right">
            {{ submit_button("Зберегти", "class": "btn btn-success") }}
        </li>
    </ul>

    {{ content() }}

    <div class="center scaffold">

        <h2>Завантаження документу</h2>

        <ul class="nav nav-tabs">
            <li class="active"><a href="#A" data-toggle="tab">Basic</a></li>
            <li><a href="#B" data-toggle="tab">Users</a></li>
        </ul>

        <div class="tabbable">
            <div class="tab-content">
                <div class="tab-pane active" id="A">

                    <div class="clearfix">
                        <label for="file">Файл</label>
                        {{ form.render("file") }}
                    </div>

                    <div class="clearfix">
                        <label for="important">Важлива інформація?</label>
                        {{ form.render("important") }}
                    </div>

                </div>

                <div class="tab-pane" id="B">
                    <div class="clearfix">
                        <label for="description">Опис</label>
                        {{ form.render("description") }}
                    </div>
                </div>

            </div>
        </div>

    </div>

</form>
</div>