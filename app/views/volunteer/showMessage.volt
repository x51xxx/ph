{{ content() }}

<h3>{{ title }}</h3>
<br>
{% if page.items != null %}
<div class="col-sm-12 row">
    <div class="pull-left" style="padding-left: 20px">
        <form method="post" action="{{ url('volunteer/showMessage/' ~ type) }}" autocomplete="off">
            <div class="row">
                <div class="clearfix pull-left" style="padding: 0 25px;">
                    <label for="pollNumber">Введіть номер дільниці<span style="padding-left: 10px">{{ form.render("pollNumber") }}</span></label>
                </div>
                <div class="pull-left">
                    {{ submit_button("Фільтрувати", "class": "btn btn-success") }}
                </div>
            </div>
        </form>
    </div>
    <div class="pull-right">
        <a href="{{ url('volunteer/generateExcel/' ~ type) }}"><button class="btn btn-success">Експортувати дані</button></a>
    </div>
</div>
{% endif %}

{% for message in page.items %}
{% if loop.first %}
    <table class="table table-bordered table-striped" align="center">
    {% if type != 'violations' and type != 'startCalculation' and type != 'finalVotes' %}
        <thead>
        <tr>
            <th>Дільниця</th>
            <th>Явка</th>
            <th>Час</th>
            <th>Спостерігач</th>
        </tr>
        </thead>
    {% elseif type != 'finalVotes' %}
        {% if type == 'startCalculation' %}
            <thead>
            <tr>
                <th>Дільниця</th>
                <th>Час</th>
                <th>Спостерігач</th>
            </tr>
            </thead>
        {% else %}
            <thead>
            <tr>
                <th>Дільниця</th>
                <th>Час</th>
                <th>Сторонні особи</th>
                <th>Опис порушення</th>
                <th>Спостерігач</th>
            </tr>
            </thead>
        {% endif %}
    {% else %}
        <thead>
        <tr>
            <th>Дільниця</th>
            <th>Богомолець О.В.</th>
            <th>Гриценко А.С.</th>
            <th>Добкін М.М.</th>
            <th>Ляшко O.В.</th>
            <th>Порошенко П.О.</th>
            <th>Тимошенко Ю.В.</th>
            <th>Тігіпко С.Л.</th>
        </tr>
        </thead>
    {% endif %}
{% endif %}
    {% if type != 'violations' and type != 'startCalculation' and type != 'finalVotes' %}
        <tbody>
        <tr>
            <td title="{{ message.polls.address }}">{{ message.polls.name }}</td>
            <td>{{ message.quantity }}</td>
            <td>{{  message.hour }}</td>
            <td>{{ message.users.name }}</td>
        </tr>
        </tbody>
    {% elseif type != 'finalVotes' %}
        {% if type == 'startCalculation' %}
            <tbody>
            <tr>
                <td title="{{ message.polls.address }}">{{ message.polls.name }}</td>
                <td>{{ date('H:i', message.time) }}</td>
                <td>{{ message.users.name }}</td>
            </tr>
            </tbody>
        {% else %}
            <tbody>
            <tr>
                <td title="{{ message.polls.address }}">{{ message.polls.name }}</td>
                <td>{{ date('H:i', message.time) }}</td>
                {% if message.isUnauthorizedPersons == 'Y' %}
                    <td>Так</td>
                {% else %}
                    <td></td>
                {% endif %}
                <td>{{ message.violationDescription }}</td>
                <td>{{ message.users.name }}</td>
            </tr>
            </tbody>
        {% endif %}
    {% else %}
        <tbody>
        <tr>
            <td title="{{ message[1] }}">{{ message[0] }}</td>
            <td>{{ message[2] }}</td>
            <td>{{ message[3] }}</td>
            <td>{{ message[4] }}</td>
            <td>{{ message[5] }}</td>
            <td>{{ message[6] }}</td>
            <td>{{ message[7] }}</td>
            <td>{{ message[8] }}</td>
        </tr>
        </tbody>
    {% endif %}
{% if loop.last %}
    <tbody>
    <tr>
        <td colspan="10" align="right">
            <div class="btn-group">
                {{ link_to("volunteer/showMessage/" ~ type, '<i class="icon-fast-backward"></i> Перша', "class": "btn") }}
                {{ link_to("volunteer/showMessage/" ~ type ~ "/" ~ page.before, '<i class="icon-step-backward"></i> Попередня', "class": "btn ") }}
                {{ link_to("volunteer/showMessage/" ~ type ~ "/" ~ page.next, '<i class="icon-step-forward"></i> Наступна', "class": "btn") }}
                {{ link_to("volunteer/showMessage/" ~ type ~ "/" ~ page.last, '<i class="icon-fast-forward"></i> Остання', "class": "btn") }}
            </div>
            <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span>
        </td>
    </tr>
    <tbody>
</table>
{% endif %}
{% endfor %}
{% if page.items == null %}
<div>На даний момент не зареєстровано жодного повідомлення</div>
{% endif %}