<form method="post" action="{{ url('volunteer/addMessage') }}" autocomplete="off">

    <ul class="pager">
        {#<li class="previous pull-left">#}
            {#{{ link_to("profiles", "&larr; Повернутись") }}#}
        {#</li>#}
        <li class="pull-right">
            {{ submit_button("Зберегти", "class": "btn btn-large btn-success") }}
        </li>
    </ul>

    {{ content() }}

    <div class="center scaffold">

        <h2>Повідомлення</h2>

        <div class="clearfix">
            <label for="type">Тип повідомлення</label>
            {{ form.render("type") }}
        </div>

        <div class="clearfix hidden" id="votersQty">
            <label for="quantity">Явка</label>
            {{ form.render("quantity") }}
        </div>

        <div class="clearfix hidden" id="hourlyTime">
            <label for="timeAt">Час</label>
            {{ form.render("timeAt") }}
        </div>

        <div class="clearfix hidden" id="closedTime">
            <label for="time">Час (Години : Хвилини)</label>
            {{ form.render("hours") }}&nbsp;<b>:</b>&nbsp;{{ form.render("minutes") }}
        </div>

        <div class="clearfix hidden" id="poolsNum">
            <label for="pollsId">Дільниця</label>
            {{ form.render("pollsId") }}
        </div>

        <div class="clearfix hidden" id="unAuth">
            <label for="unauthorizedPersons">{{ form.render("unauthorizedPersons") }} Наявність посторонніх осіб</label>
        </div>

        <div class="clearfix hidden" id="violDesc">
            <label for="type">Опис</label>
            {{ form.render("description") }}
        </div>

        <div class="clearfix hidden" id="candidates">
            <table border="0">
                <tr>
                    <td><label for="votesTUV"><b>Тимошенко Юлія Володимирівна</b></label></td>
                    <td>{{ form.render("votesTUV") }}</td>
                </tr>
                <tr>
                    <td><label for="votesPPO"><b>Порошенко Петро Олексійович</b></label></td>
                    <td>{{ form.render("votesPPO") }}</td>
                </tr>
                <tr>
                    <td><label for="votesLOV"><b>Ляшко Олег Валерійович</b></label></td>
                    <td>{{ form.render("votesLOV") }}</td>
                </tr>
                <tr>
                    <td><label for="votesGAS"><b>Гриценко Анатолій Степанович</b></label></td>
                    <td>{{ form.render("votesGAS") }}</td>
                </tr>
                <tr>
                    <td><label for="votesBOV"><b>Богомолець Ольга Вадимівна</b></label></td>
                    <td>{{ form.render("votesBOV") }}</td>
                </tr>
                <tr>
                    <td><label for="votesDMM"><b>Добкін Михайло Маркович</b></label></td>
                    <td>{{ form.render("votesDMM") }}</td>
                </tr>
                <tr>
                    <td><label for="votesTSL"><b>Тігіпко Сергій Леонідович</b></label></td>
                    <td>{{ form.render("votesTSL") }}</td>
                </tr>
            </table>
        </div>

    </div>

</form>
