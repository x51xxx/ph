<?php
namespace Elections\Forms;

use Elections\Models\Polls;
use Elections\Models\Users;
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\File;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Confirmation;
use Phalcon\Validation\Validator\Regex;

class SignUpForm extends Form
{

    public function initialize($entity = null, $options = null)
    {
        $name = new Text('name');

        $name->setLabel('Прізвище, ім’я, по батькові');

        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'Прізвище, ім’я, по батькові є обов\'язковим'
            ))
        ));

        $this->add($name);

        // Email
        $email = new Text('email');

        $email->setLabel('E-Mail');

        $email->addValidators(array(
            new PresenceOf(array(
                'message' => 'e-mail є обов\'язковим'
            )),
            new Email(array(
                'message' => 'e-mail є обов\'язковим'
            ))
        ));

        $this->add($email);


        $phone = new Text('phone');

        $phone->setLabel('Телефон');

        $phone->addValidators(array(
            new PresenceOf(array(
                'message' => 'Телефон є обов\'язковим полем'
            ))
        ));
        $phone->addValidator(new Regex(array(
            'pattern' => '/\+38\d{10}/',
            'message' => 'Номер телефону повинен бути у форматі +38xxxxxxxxxx'
        )));
        $phone->setDefault('+380');
        $this->add($phone);


        $addressOfResidence = new TextArea('addressOfResidence');

        $addressOfResidence->setLabel('Адреса місця проживання');

        $addressOfResidence->addValidators(array(
            new PresenceOf(array(
                'message' => 'Адреса місця проживання є обов\'язковим полем'
            ))
        ));

        $this->add($addressOfResidence);


        $addressOfResidence = new TextArea('addressOfResidence');
        $addressOfResidence->setLabel('Адреса місця проживання');
        $addressOfResidence->addValidators(array(
            new PresenceOf(array(
                'message' => 'Адреса місця проживання є обов\'язковим полем'
            ))
        ));

        $this->add($addressOfResidence);

        $placeOfWork = new TextArea('placeOfWork');
        $placeOfWork->setLabel('Місце роботи');
        $placeOfWork->addValidators(array(
            new PresenceOf(array(
                'message' => 'Місце роботи є обов\'язковим полем'
            ))
        ));

        $this->add($placeOfWork);


        $work = new Text('work');

        $work->setLabel('Посада (заняття)');

        $work->addValidators(array(
            new PresenceOf(array(
                'message' => 'Посада (заняття) є обов\'язковим полем'
            ))
        ));

        $this->add($work);

        $birthday = new Date('birthday');
        $birthday->setLabel('Дата народження');
        $birthday->addValidators(array(
            new PresenceOf(array(
                'message' => 'Дата народження є обов\'язковим полем'
            ))
        ));
        $this->add($birthday);

        // Password
        $password = new Password('password');

        $password->setLabel('Пароль');

        $password->addValidators(array(
            new PresenceOf(array(
                'message' => 'The password is required'
            )),
            new StringLength(array(
                'min' => 4,
                'messageMinimum' => 'Пароль занадто короткий. Мінімум 4 символів'
            )),
            new Confirmation(array(
                'message' => 'Пароль не відповідає підтвердження',
                'with' => 'confirmPassword'
            ))
        ));

        $this->add($password);

        // Confirm Password
        $confirmPassword = new Password('confirmPassword');

        $confirmPassword->setLabel('Підтвердження паролю');

        $confirmPassword->addValidators(array(
            new PresenceOf(array(
                'message' => 'The confirmation password is required'
            ))
        ));

        $this->add($confirmPassword);


        $file = new File('passport_scan1', array( ));
        $file->setLabel('Скан паспорту 1 сторінка');
        $this->add($file);


        $file = new File('passport_scan2', array( ));
        $file->setLabel('Скан паспорту 2 сторінка');
        $this->add($file);


        $polls = array();
        foreach (Polls::find('active = "Y"') as $pool) {
            $polls[$pool->id] = $pool->district . ' округ, дільниця №' . $pool->name . '  (' . $pool->address . ')';
        }

        $pollsId = new Select('pollsId', $polls, array(
            'using' => array(
                'id',
                'name'
            ),
        ));
        $pollsId->setLabel('Дільниця');

        $pollsId->addValidator(new PresenceOf(array(
            'message' => 'Виберіть дільницю'
        )));
        $this->add($pollsId);



        // Remember
        $terms = new Check('terms', array(
            'value' => 'yes'
        ));

        $terms->setLabel('Приймаю Правила та умови');

        $terms->addValidator(new Identical(array(
            'value' => 'yes',
            'message' => 'Умови повинні бути прийняті'
        )));

        $this->add($terms);

        // CSRF
        $csrf = new Hidden('csrf');

        $csrf->addValidator(new Identical(array(
            'value' => $this->security->getSessionToken(),
            'message' => 'CSRF validation failed'
        )));

        $this->add($csrf);

        // Sign Up
        $this->add(new Submit('Зареєструватись', array(
            'class' => 'btn btn-success'
        )));
    }

    /**
     * Prints messages for a specific element
     */
    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }
}
