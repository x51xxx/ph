<?php
namespace Elections\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Identical;

class LoginForm extends Form
{

    public function initialize()
    {
        // Email
        $email = new Text('email', array(
            'placeholder' => 'Телефон або E-Mail'
        ));

        $email->addValidators(array(
            new PresenceOf(array(
                'message' => 'Електронна пошта або номер телефону є обов\'язковим полим'
            )),
        ));
        $email->setDefault("+380");
        $this->add($email);

        // Password
        $password = new Password('password', array(
            'placeholder' => 'Пароль'
        ));

        $password->addValidator(new PresenceOf(array(
            'message' => 'Пароль є обов\'язковим полим'
        )));

        $this->add($password);

        // Remember
        $remember = new Check('remember', array(
            'value' => 'yes'
        ));

        $remember->setLabel('Запам\'ятати мене');

        $this->add($remember);

        // CSRF
        $csrf = new Hidden('csrf');

        $csrf->addValidator(new Identical(array(
            'value' => $this->security->getSessionToken(),
            'message' => 'CSRF validation failed'
        )));

        $this->add($csrf);

        $this->add(new Submit('go', array(
            'class' => 'btn btn-success'
        )));
    }
}
