<?php

namespace Elections\Forms;


use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Form;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\Identical;

/**
 * Class MessageFilterForm
 * @namespace Elections\Forms
 */
class MessageFilterForm extends Form
{
    public function initialize()
    {
        $pollNumber = new Text('pollNumber', array('placeholder' => '260872'));
        $pollNumber->addValidator(new PresenceOf(array(
            'message' => 'Введіть номер дільниці'
        )));
        $pollNumber->addValidator(new Regex(array(
            'pattern' => '/\d{6}/',
            'message' => 'Введіть корректний номер дільниці'
        )));
        $this->add($pollNumber);


        // CSRF
        $csrf = new Hidden('csrf');

        $csrf->addValidator(new Identical(array(
            'value' => $this->security->getSessionToken(),
            'message' => 'CSRF validation failed'
        )));

        $this->add($csrf);

        $this->add(new Submit('go', array(
            'class' => 'btn btn-success'
        )));
    }
} 