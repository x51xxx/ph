<?php
namespace Elections\Forms;

use Elections\Models\Polls;
use Elections\Models\Users;
use Elections\Models\Profiles;
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\File;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Confirmation;
use Phalcon\Validation\Validator\Regex;


class UsersForm extends Form
{

    public function initialize($entity = null, $options = null)
    {

        // In edition the id is hidden
        if (isset($options['edit']) && $options['edit']) {
            $id = new Hidden('id');
        } else {
            $id = new Text('id');
        }

        $this->add($id);

        $name = new Text('name', array(
            'placeholder' => 'Name'
        ));

        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'Ім\'я є обов\'язковим полим'
            ))
        ));

        $this->add($name);

        $email = new Text('email', array(
            'placeholder' => 'Email'
        ));

        $email->addValidators(array(
            new PresenceOf(array(
                'message' => 'The e-mail is required'
            )),
            new Email(array(
                'message' => 'The e-mail is Ніt valid'
            ))
        ));

        $this->add($email);

        $this->add(new Select('profilesId', Profiles::find('active = "Y"'), array(
            'using' => array(
                'id',
                'name'
            ),
            'useEmpty' => true,
            'emptyText' => '...',
            'emptyValue' => ''
        )));


        $phone = new Text('phone');

        $phone->setLabel('Телефон');

        $phone->addValidators(array(
            new PresenceOf(array(
                'message' => 'Телефон є обов\'язковим полем'
            ))
        ));
        $phone->addValidator(new Regex(array(
            'pattern' => '/\+38\d{10}/',
            'message' => 'Номер телефону повинен бути у форматі +38xxxxxxxxxx'
        )));
        $phone->setDefault('+380');
        $this->add($phone);


        $addressOfResidence = new TextArea('addressOfResidence');

        $addressOfResidence->setLabel('Адреса місця проживання');

        $addressOfResidence->addValidators(array(
            new PresenceOf(array(
                'message' => 'Адреса місця проживання є обов\'язковим полем'
            ))
        ));

        $this->add($addressOfResidence);


        $addressOfResidence = new TextArea('addressOfResidence');
        $addressOfResidence->setLabel('Адреса місця проживання');
        $addressOfResidence->addValidators(array(
            new PresenceOf(array(
                'message' => 'Адреса місця проживання є обов\'язковим полем'
            ))
        ));

        $this->add($addressOfResidence);

        $placeOfWork = new TextArea('placeOfWork');
        $placeOfWork->setLabel('Місце роботи');
        $placeOfWork->addValidators(array(
            new PresenceOf(array(
                'message' => 'Місце роботи є обов\'язковим полем'
            ))
        ));

        $this->add($placeOfWork);


        $work = new Text('work');

        $work->setLabel('Посада (заняття)');

        $work->addValidators(array(
            new PresenceOf(array(
                'message' => 'Посада (заняття) є обов\'язковим полем'
            ))
        ));

        $this->add($work);

        $birthday = new Date('birthday');
        $birthday->setLabel('Дата народження');
        $birthday->addValidators(array(
            new PresenceOf(array(
                'message' => 'Дата народження є обов\'язковим полем'
            ))
        ));
        $this->add($birthday);


        $polls = array();
        foreach (Polls::find('active = "Y"') as $pool) {
            $polls[$pool->id] = $pool->district . ' округ, дільниця №' . $pool->name . '  (' . $pool->address . ')';
        }

        $pollsId = new Select('pollsId', $polls, array(
            'using' => array(
                'id',
                'name'
            ),
            'useEmpty' => true,
            'emptyText' => '...',
            'emptyValue' => ''
        ));
        $pollsId->setLabel('Дільниця');

        $pollsId->addValidator(new PresenceOf(array(
            'message' => 'Виберіть дільницю'
        )));
        $this->add($pollsId);




        $this->add(new Select('banned', array(
            'Y' => 'Так',
            'N' => 'Ні'
        )));

        $this->add(new Select('suspended', array(
            'Y' => 'Так',
            'N' => 'Ні'
        )));

        $this->add(new Select('active', array(
            'Y' => 'Так',
            'N' => 'Ні'
        )));
    }
}
