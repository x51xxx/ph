<?php
namespace Elections\Forms;

use Elections\Models\EventType;
use Elections\Models\Polls;
use Elections\Models\Users;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Identical;

class PhotoForm extends Form
{

    public function initialize()
    {
        $identity = $this->auth->getIdentity();

        $file = new File('file');
        $this->add($file);

        $description = new TextArea('description', array(
            'placeholder' => 'Опис',
            "cols" => 40,
            "rows" => 16,
            "class" => ""
        ));

        $this->add($description);


        $important = new Check('important', array(
            'value' => 'yes'
        ));

        $important->setLabel('Важливе повідомлення');

        $this->add($important);


        $this->add(new Select('eventTypeId', EventType::find('active = "Y"'), array(
            'using' => array(
                'id',
                'name'
            ),
            'useEmpty' => true,
            'emptyText' => '...',
            'emptyValue' => ''
        )));

        $polls = array();
        foreach (Polls::find('active = "Y"') as $pool) {
            $polls[$pool->id] = $pool->district . ' округ, дільниця №' . $pool->name . '  (' . $pool->address . ')';
        }

        $user = Users::findFirst($identity['id']);
        $defaultPollId = '';
        if ($user->poll)
            $defaultPollId = $user->poll->id;

        $pollsId = new Select('pollsId', $polls, array(
            'using' => array(
                'id',
                'name'
            ),
        ));
        $pollsId->setDefault($defaultPollId);
        $this->add($pollsId);

        // CSRF
        $csrf = new Hidden('csrf');

        $csrf->addValidator(new Identical(array(
            'value' => $this->security->getSessionToken(),
            'message' => 'CSRF validation failed'
        )));

        $this->add($csrf);

        $this->add(new Submit('завантажити', array(
            'class' => 'btn btn-success'
        )));
    }
}
