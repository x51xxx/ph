<?php
namespace Elections\Forms;


use Elections\Models\Polls;
use Elections\Models\Users;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Form;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Regex;

/**
 * Class MessageForm
 * @namespace Elections\Forms
 */
class MessageForm extends Form
{
    public function initialize()
    {
        $identity = $this->auth->getIdentity();

        $messageType = new Select('type',
            array(
                'hourlyQuantity' => 'Щогодинна явка',
                'finalResult' => 'Кількість зареєстрованих виборців',
                'nonVoted' => 'Кількість виборців які не змогли проголосувати',
                'homeVoted' => 'Кількість виборців які проголосували вдома',
                'startCalculation' => 'Початок підрахунку голосів',
                'violations' => 'Порушення',
                'finalVotes' => 'Результати голосів по кандидатах'
            ),
            array(
                'using' => array(
                    'id',
                    'name'
                ),
                'useEmpty' => true,
                'emptyText' => '...',
                'emptyValue' => ''
            )
        );
        $messageType->addValidator(new PresenceOf(array(
            'message' => 'Виберіть тип повідомлення'
        )));
        $this->add($messageType);

        $quantity = new Text('quantity');
        $quantity->addValidator(new PresenceOf(array(
            'message' => 'Явка є обов\'язкова для введення!'
        )));
        $quantity->addValidator(new Regex(array(
            'pattern' => '/[0-9]*/',
            'message' => 'Явка має бути цілим числом'
        )));
        $this->add($quantity);

        $timeAt = new Select('timeAt',
            array(
                '8' => '8:00',
                '9' => '9:00',
                '10' => '10:00',
                '11' => '11:00',
                '12' => '12:00',
                '13' => '13:00',
                '14' => '14:00',
                '15' => '15:00',
                '16' => '16:00',
                '17' => '17:00',
                '18' => '18:00',
                '19' => '19:00',
                '20' => '20:00',
                '21' => '21:00',
                '22' => '22:00',
            ),
            array(
                'using' => array(
                    'id',
                    'name'
                ),
                'useEmpty' => true,
                'emptyText' => '...',
                'emptyValue' => ''
            )
        );
        $timeAt->addValidator(new PresenceOf(array('message' => 'Виберіть час')));
        $this->add($timeAt);

        $hour = new Text('hours', array(
            'placeholder' => '22',
            'style' => 'Width: 40px'
        ));
        $currentHour = new \DateTime('now', new \DateTimeZone('Europe/Kiev'));
        $hour->setDefault($currentHour->format('H'));
        $hour->addValidator(new PresenceOf(array(
            'message' => 'Година є обов\'язкова для введення!'
        )));
        $hour->addValidator(new Regex(array(
            'pattern' => '/([0-1][0-9]|2[0-3])|([0-9])/',
            'message' => 'Введіть коректну годину'
        )));
        $this->add($hour);

        $minutes = new Text('minutes', array(
            'placeholder' => '00',
            'style' => 'Width: 40px'
        ));
        $minutes->setDefault('00');
        $minutes->addValidator(new PresenceOf(array(
            'message' => 'Хвилини є обов\'язкова для введення!'
        )));
        $minutes->addValidator(new Regex(array(
            'pattern' => '/[0-5][0-9]/',
            'message' => 'Введіть коректні хвилини'
        )));
        $this->add($minutes);

        $polls = array();
        foreach (Polls::find('active = "Y"') as $pool) {
            $polls[$pool->id] = $pool->district . ' округ, дільниця №' . $pool->name . '  (' . $pool->address . ')';
        }

        $user = Users::findFirst($identity['id']);
        $defaultPollId = '';
        if ($user->poll)
            $defaultPollId = $user->poll->id;

        $pollsId = new Select('pollsId', $polls, array(
            'using' => array(
                'id',
                'name'
            ),
        ));
        $pollsId->setDefault($defaultPollId);

        $pollsId->addValidator(new PresenceOf(array(
            'message' => 'Виберіть дільницю'
        )));
        $this->add($pollsId);

        $unauthorizedPersons = new Check('unauthorizedPersons', array(
            'value' => 'Y'
        ));
        $this->add($unauthorizedPersons);

        $description = new TextArea('description', array(
            'placeholder' => 'Опис',
            "cols" => 40,
            "rows" => 7,
            "class" => ""
        ));
        $description->addValidator(new PresenceOf(array(
            'message' => 'Опишіть порушення'
        )));
        $this->add($description);

        $votesBOV = new Text('votesBOV');
        $votesBOV->setAttributes(array(
            'placeholder' => '100',
            'style' =>'width: 50px; margin-left: 20px'
        ));
        $votesBOV->addValidator(new PresenceOf(array(
            'message' => 'Введіть кількість голосів для кандидата Богомолець О. В.'
        )));
        $votesBOV->addValidator(new Regex(array(
            'pattern' => '/\d*/',
            'message' => 'Введіть правильне значення кількості голосів для кандидата Богомолець О. В.'
        )));
        $this->add($votesBOV);

        $votesGAS = new Text('votesGAS');
        $votesGAS->setAttributes(array(
            'placeholder' => '100',
            'style' =>'width: 50px; margin-left: 20px'
        ));
        $votesGAS->addValidator(new PresenceOf(array(
            'message' => 'Введіть кількість голосів для кандидата Гриценко А. С.'
        )));
        $votesGAS->addValidator(new Regex(array(
            'pattern' => '/\d*/',
            'message' => 'Введіть правильне значення кількості голосів для кандидата Гриценко А. С.'
        )));
        $this->add($votesGAS);

        $votesDMM = new Text('votesDMM');
        $votesDMM->setAttributes(array(
            'placeholder' => '100',
            'style' =>'width: 50px; margin-left: 20px'
        ));
        $votesDMM->addValidator(new PresenceOf(array(
            'message' => 'Введіть кількість голосів для кандидата Добкін М. М.'
        )));
        $votesDMM->addValidator(new Regex(array(
            'pattern' => '/\d*/',
            'message' => 'Введіть правильне значення кількості голосів для кандидата Добкін М. М.'
        )));
        $this->add($votesDMM);

        $votesLOV = new Text('votesLOV');
        $votesLOV->setAttributes(array(
            'placeholder' => '100',
            'style' =>'width: 50px; margin-left: 20px'
        ));
        $votesLOV->addValidator(new PresenceOf(array(
            'message' => 'Введіть кількість голосів для кандидата Ляшко О. В.'
        )));
        $votesLOV->addValidator(new Regex(array(
            'pattern' => '/\d*/',
            'message' => 'Введіть правильне значення кількості голосів для кандидата Ляшко О. В.'
        )));
        $this->add($votesLOV);

        $votesPPO = new Text('votesPPO');
        $votesPPO->setAttributes(array(
            'placeholder' => '100',
            'style' =>'width: 50px; margin-left: 20px'
        ));
        $votesPPO->addValidator(new PresenceOf(array(
            'message' => 'Введіть кількість голосів для кандидата Порошенко П. О.'
        )));
        $votesPPO->addValidator(new Regex(array(
            'pattern' => '/\d*/',
            'message' => 'Введіть правильне значення кількості голосів для кандидата Порошенко П. О.'
        )));
        $this->add($votesPPO);

        $votesTUV = new Text('votesTUV');
        $votesTUV->setAttributes(array(
            'placeholder' => '100',
            'style' =>'width: 50px; margin-left: 20px'
        ));
        $votesTUV->addValidator(new PresenceOf(array(
            'message' => 'Введіть кількість голосів для кандидата Тимошенко Ю. В.'
        )));
        $votesTUV->addValidator(new Regex(array(
            'pattern' => '/\d*/',
            'message' => 'Введіть правильне значення кількості голосів для кандидата Тимошенко Ю. В'
        )));
        $this->add($votesTUV);

        $votesTSL = new Text('votesTSL');
        $votesTSL->setAttributes(array(
            'placeholder' => '100',
            'style' =>'width: 50px; margin-left: 20px'
        ));
        $votesTSL->addValidator(new PresenceOf(array(
            'message' => 'Введіть кількість голосів для кандидата Тігіпко С. Л.'
        )));
        $votesTSL->addValidator(new Regex(array(
            'pattern' => '/\d*/',
            'message' => 'Введіть правильне значення кількості голосів для кандидата Тігіпко С. Л.'
        )));
        $this->add($votesTSL);

        // CSRF
        $csrf = new Hidden('csrf');

        $csrf->addValidator(new Identical(array(
            'value' => $this->security->getSessionToken(),
            'message' => 'CSRF validation failed'
        )));

        $this->add($csrf);

        $this->add(new Submit('go', array(
            'class' => 'btn btn-success'
        )));
    }
} 